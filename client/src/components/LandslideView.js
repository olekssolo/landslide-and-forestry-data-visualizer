import { useEffect, useState } from 'react';
import LandslideHeatmap from './heatmap/LandslideHeatmap';
import '../stylesheets/View.css';
import Sidebar from './Sidebar/Sidebar';

/**
 * @description Manages the state of the landslide data as well as the yearRange that filters it
 * It is the parent component for the sidebar and heatmap components.
 * @returns A sidebar - The heatmap is updated via react components but does not return any JSX
 */
export default function LandslideView(props) {
  const [landslideData, setLandslideData] = useState([]);
  const [yearRange, setYearRange] = useState([2006, 2008]);

  useEffect(() => {
    (async function fetchData() {

      // Filter through the range and check which years are already cached
      let listOfYears = [];
      for (let year = yearRange[0]; year <= yearRange[1]; year++) {
        if (sessionStorage.getItem(`landslides_${year}`) === null) {
          listOfYears.push(year);
        }
      }
      
      // Create the 3-year indexes for fetching
      const fetchingRanges = [];
      while (listOfYears.length > 0) {
        const lowerBound = listOfYears[0];
        const upperBound = lowerBound + 2;
  
        listOfYears = listOfYears.filter((year) => year > upperBound);
        fetchingRanges.push([lowerBound, upperBound]);
      }

      let filteredData = [];
      const listOfPromises = [];
      for (const range of fetchingRanges) {
        listOfPromises.push(fetchLandslideData(range, filteredData));
      }
      await Promise.all(listOfPromises);

      // Fetches data from the landslide api for the provided range
      async function fetchLandslideData(range, filteredData) {
        try {
          const resp = await fetch(`/landslides/year/${range[0]}?endYear=${range[1]}`);
          if (resp.ok) {
            const json = await resp.json();
      
            filteredData = filteredData.concat(json);
            filterFetchedData(json, range);
          }
        } catch (error) {
          console.error(error.message);
        }
      }

      // Filters landslide data
      for (let year = yearRange[0]; year <= yearRange[1]; year++) {
        const yearData = JSON.parse(sessionStorage.getItem(`landslides_${year}`));
        if (yearData !== null) {
          if (yearData.length > 0) {
            filteredData = filteredData.concat(yearData);
          }
        }
      }

      setLandslideData(filteredData);
    })();
  }, [yearRange]);

  function filterFetchedData(json, range) {
    for (let year = range[0]; year <= range[1]; year++) {
      if (sessionStorage.getItem(`landslides_${year}`) === null) {
        if (json.length > 0) {
          const yearData = json.filter((item) => item.year === year);
          sessionStorage.setItem(`landslides_${year}`, JSON.stringify(yearData));
        }
      }
    }
  }

  const description = 'This heatmap represents the landslides recorded'
  + 'throughout the world by respective countries. The landslide data ranges from '
  + '1993 to 2017, while it is important to take into account that not all countries '
  + 'report landslides within those exact year and this information may not fully '
  + 'encompass all the landslides within these ranges. The data represents a story '
  + 'on the heatmap with regards to geography and climate, as well as other external factors.';

  return(
    <section className="container-view">
      <Sidebar yearRange={yearRange} setYearRange={setYearRange} dataView={props.dataView} 
        description={description}/>
      <LandslideHeatmap landslideData={landslideData}/>
    </section>
  );
}