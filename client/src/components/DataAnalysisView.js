import { useState, useEffect } from 'react';
import SideBar from './Sidebar/Sidebar';
import '../stylesheets/DataAnalysisView.css';
import '../stylesheets/View.css';
import Plotly from 'plotly.js-basic-dist-min';
import createPlotlyComponent from 'react-plotly.js/factory';

const Plot = createPlotlyComponent(Plotly);

/**
 * @description Retrieves data and creates a graphical representation of the 
 * relationship between a country's forest cover % and the number of landslides
 * per year
 * @returns {JSX} A view with the sidebar implemented and a Plotly graph
 */
export function ComparisonGraph({ dataView }) {

  const [landslideData, setLandslideData] = useState([]);
  const [forestData, setForestData] = useState([]);
  const [country, setCountry] = useState('Canada');
  const [countryList, setCountryList] = useState([]);

  const years = [];
  const sortedForestData = forestData.sort((a, b) =>{
    return a.Year - b.Year;
  });

  const forestCover = sortedForestData.map((item) => {
    if (item.Year > 1990) {
      years.push(item.Year);
    }
    return item.ForestCover;
  });

  const landslideCount = years.map((year) => {
    return landslideData.filter((obj) => obj.year === year).length;
  });

  useEffect(() => {
    (async function fetchCountries() {
      try{
        if (sessionStorage.getItem('commonCountries') === null) {
          const countryResp = await fetch('/countries/common');
          if (countryResp.ok) {
            const countryJson = await countryResp.json();
            setCountryList(countryJson);
            sessionStorage.setItem('commonCountries', JSON.stringify(countryJson));
          }
        } else {
          setCountryList(JSON.parse(sessionStorage.getItem('commonCountries')));
        }
      } catch(error) {
        console.error(error.message);
      }
      
    })();
  }, []);

  useEffect(() => {
    (async function fetchData() {
      try {
        if (sessionStorage.getItem(`landslides_${country}`) === null) {
          const resp = await fetch(`/landslides/country/${country}`);
          if (resp.ok) {
            const json = await resp.json();
            setLandslideData(json);
            sessionStorage.setItem(`landslides_${country}`, JSON.stringify(json));
          }
        } else {
          setLandslideData(JSON.parse(sessionStorage.getItem(`landslides_${country}`)));
        }
  
        if (sessionStorage.getItem(`forestry_${country}`) === null) {
          const forestResp = await fetch(`/forestry/country/${country}`);
          if (forestResp.ok) {
            const forestJson = await forestResp.json();
            setForestData(forestJson);
            sessionStorage.setItem(`forestry_${country}`, JSON.stringify(forestJson));
          }
        } else {
          setForestData(JSON.parse(sessionStorage.getItem(`forestry_${country}`)));
        }
      } catch(error) {
        console.error(error.message);
      }
      
    })();
  }, [country]);

  const graphTitle = window.innerWidth >= 700 ?
    `<b>Landslides vs Forest Cover in ${country}</b>` :
    `<b>Landslides vs Forest Cover<br>in ${country}</b>`;

  const description = 'This graph is meant to represent the relationship between the ' 
  + 'number of landslides and the forest cover in a given country. We had hypothesized ' 
  + 'that a reported decrease in forest cover would lead to and increase in ' 
  + 'the number of landslides. Unfortunately, the data '
  + 'for landslides is not consistently reported across the globe leading to many countries where '
  + 'there is no conclusion to be drawn from the comparison. Another limitation of this analysis '
  + 'is that it only considers the percentage of the country\'s land covered in forestry and no '
  + 'other factors.';
  
  return(
    <>
      <SideBar dataView={dataView} setCountry={setCountry} description={description}
        commonCountries={countryList}/>
      <Plot
        useResizeHandler
        data={[
          {type: 'bar', 
            x: years, 
            y: landslideCount,
            marker: {color: '#66411c'},
            name : 'Number of Landslides',
            
          },
          {
            x: years,
            y: forestCover,
            type: 'scatter',
            mode: 'lines+markers',
            marker: {color: 'green', size: 12},
            name: 'Forest Cover %',
            yaxis: 'y2'
          },
        ]}
        layout={{
          autosize: true,
          showlegend: true,
          legend: {'orientation': 'h'},
          paper_bgcolor: '#C8AE95',
          plot_bgcolor: '#f5e0cb',
          title: {text: graphTitle,
            font: {weight: 500}},
          yaxis: {
            automargin: true,
            title: {text: '<b>Number of Landslides</b>', 
              font: {color: '#66411c'}},
            tickfont: {color: '#362312'},
          },
          yaxis2: {
            automargin: true,
            title: {text: `<b>Forest Cover %</b> (of Land Covered)`, 
              font : {color: 'green'} },
            tickfont: {color: '#362312'},
            overlaying: 'y',
            side: 'right',
            dtick: 1,
            tick0: 0,
            autotick: true
          }} 
        }
      />
    </>
  );
}
