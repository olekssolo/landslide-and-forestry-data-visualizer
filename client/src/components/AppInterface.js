import { useState } from 'react';
import '../stylesheets/AppInterface.css';
import LandslideView from './LandslideView';
import { ForestryGraph } from './ForestryGraph';
import { ComparisonGraph } from './DataAnalysisView';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHillRockslide, faTree, faMagnifyingGlassChart } from '@fortawesome/free-solid-svg-icons';

/**
 * @description The application interface, it manages the navigation and view state
 * @returns A navigation bar and view of the selected view state
 */
export default function AppInterface() {
  const [dataView, setDataView] = useState('landslide');
  const [landslideJSX, setLandslideJSX] = useState('Landslide Data');
  const [forestJSX, setForestJSX] = useState('Forest Data');
  const [analysisJSX, setAnalysisJSX] = useState('Analysis Data');

  const handleViewChange = (display) => {
    const map = document.getElementById('map');
    if (map !== null && map !== undefined) {
      map.style.display = display;
    }
  };

  // Changes the displayed title based on viewport size
  window.addEventListener('resize', () => {
    handleWindowWidth();
  });

  // This handles the changing of window size - Should the window width decrease below 500 pixels
  // this code will replace the text with icons that represent each view
  const handleWindowWidth = () => {

    if (document.documentElement.clientWidth < 500 && landslideJSX === 'Landslide Data' 
    && forestJSX === 'Forest Data' && analysisJSX === 'Analysis Data') {
      
      setLandslideJSX(<FontAwesomeIcon icon={faHillRockslide} size="1x"/>);
      setForestJSX(<FontAwesomeIcon icon={faTree} size="1x"/>);
      setAnalysisJSX(<FontAwesomeIcon icon={faMagnifyingGlassChart} size="1x"/>);

    } else if (document.documentElement.clientWidth > 500 && landslideJSX !== 'Landslide Data' 
    && forestJSX !== 'Forest Data' && analysisJSX !== 'Analysis Data') {

      setLandslideJSX('Landslide Data'); 
      setForestJSX('Forest Data'); 
      setAnalysisJSX('Analysis Data'); 
      
    }
  };

  // Causes a load event one the screen has calculated its width
  window.addEventListener('load', () => {
    handleWindowWidth();
  });

  handleWindowWidth();

  return(
    <section id="interface-section">
      <div className="navigation-bar">
        {dataView === 'landslide' ?
          <h1 className="navigation-button" id="landslide-view-selected">{landslideJSX}</h1>
          :
          <h1 className="navigation-button" id="landslide-view-unselected"
            onClick={() => {
              setDataView('landslide');
              handleViewChange('block');
            }}>{landslideJSX}</h1>
        }
        {dataView === 'forestcover' ?
          <h1 className="navigation-button" id="forestcover-view-selected">{forestJSX}</h1>
          :
          <h1 className="navigation-button" id="forestcover-view-unselected"
            onClick={() => {
              setDataView('forestcover');
              handleViewChange('none');
            }}>{forestJSX}</h1>
        }
        {dataView === 'analysis' ?
          <h1 className="navigation-button" id="analysis-view-selected">{analysisJSX}</h1>
          :
          <h1 className="navigation-button" id="analysis-view-unselected"
            onClick={() => {
              setDataView('analysis');
              handleViewChange('none');
            }}>{analysisJSX}</h1>
        }
      </div>
      <div id="view-container">
        {dataView === 'landslide' ?
          <span className="landslide-container">
            <LandslideView dataView={dataView}/>
            <div id="map"></div>
          </span>
          :
          <></>
        }
        {dataView === 'forestcover' ?
          <ForestryGraph dataView={dataView}/>
          :
          <></>
        }
        {dataView === 'analysis' ?
          <ComparisonGraph dataView={dataView}/>
          :
          <></>
        }
      </div>
    </section>
  );
}