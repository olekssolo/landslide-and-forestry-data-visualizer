/**
 * @description Uses the leaflet-heat library to create an interactive map with data points
 * @param landslideData The landslide data containing information pertaining to a list of landslides
 */
export default function LandslideHeatmap(props) {
  const mapObj = document.getElementById('map');
  const containerObj = document.getElementsByClassName('landslide-container')[0];
  if (mapObj !== null && mapObj !== undefined && containerObj !== null 
    && containerObj !== undefined) {
    // Removes the old map points to allow the re-render
    var container = L.DomUtil.get('map');
    if (container !== null) {
      container._leaflet_id = null;
    }

    // Clean up the old map object
    const oldMap = document.getElementById('map');
    if (oldMap !== undefined && oldMap !== null) {
      oldMap.remove();
    }

    const mapDiv = document.createElement('div');
    mapDiv.id = 'map';
    document.getElementsByClassName('landslide-container')[0].appendChild(mapDiv);

    // The heatmap creation and rendering
    var map = L.map('map').setView([45.5019, -73.5674], 12);
    map.setZoom(4);

    // Parse the landslide data
    if (props.landslideData !== null && props.landslideData !== undefined) {
      if (props.landslideData.length > 0) {
        const coordinates = props.landslideData.map((item) => {
          return [item.coordinates[1], item.coordinates[0], 1];
        });

        L.heatLayer(coordinates, {
          radius: 25, 
          blur: 10, 
          gradient: {
            0.05: 'blue', 
            0.12: 'cyan',
            0.18: 'lime',
            0.25: 'yellow', 
            0.30: 'orange',
            0.40: 'red',
            0.70: 'purple'
          },
          minOpacity: 0.1
        }).addTo(map);
      }
    }

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
      noWrap: true
    }).addTo(map);
  }
}