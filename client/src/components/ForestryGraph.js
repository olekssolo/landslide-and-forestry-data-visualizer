import { useEffect, useState } from 'react';
import Sidebar from './Sidebar/Sidebar.js';
import '../stylesheets/View.css';

import Plotly from 'plotly.js-basic-dist-min';
import createPlotlyComponent from 'react-plotly.js/factory';

const Plot = createPlotlyComponent(Plotly);

/**
 * @description Fetches data from the database and creates a graph showing
 * a country's forest cover percentage overtime (line chart), as well as the
 * country's afforestation and deforestation rates (bar chart)
 * @returns {JSX} A view with the sidebar implemented and a line + bar chart
 */
export function ForestryGraph(props) {
  const [forestData, setForestData] = useState([]);
  const [country, setCountry] = useState('Canada');
  const [countryList, setCountryList] = useState([]);
  const years = [];
  const forestCovers = [];
  const afforestations = [];
  const deforestations = [];

  useEffect(() => {
    (async function fetchCountries() {
      // fetch country list and store it if necessary
      // otherwise get country list from sessionStorage
      try{
        if (sessionStorage.getItem('forestryCountries') === null) {
          const countryResp = await fetch('/countries/forestry');
          if (countryResp.ok) {
            const countryJson = await countryResp.json();
            setCountryList(countryJson);
            sessionStorage.setItem('forestryCountries', JSON.stringify(countryJson));
          }
        } else {
          setCountryList(JSON.parse(sessionStorage.getItem('forestryCountries')));
        }
      } catch(error) {
        console.error(error.message);
      }
    })();
  }, []);

  useEffect(() => {
    (async function fetchData() {
      try {
        if (sessionStorage.getItem(`forestry_${country}`) === null) {
          const forestResp = await fetch(`/forestry/country/${country}`);
          if (forestResp.ok) {
            const forestJson = await forestResp.json();
            setForestData(forestJson);
            sessionStorage.setItem(`forestry_${country}`, JSON.stringify(forestJson));
          }
        } else {
          setForestData(JSON.parse(sessionStorage.getItem(`forestry_${country}`)));
        }
      } catch(error) {
        console.error(error.message);
      }
    })();
  }, [country]);

  // sort data by year so it's properly ordered
  // and doesn't cause a multi-connection issue
  forestData.sort((a, b) => a.Year - b.Year);

  for (const info of forestData) {
    years.push(info.Year);
    forestCovers.push(info.ForestCover);
    afforestations.push(info.Afforestation);
    deforestations.push(info.Deforestation);
  }

  const afforestationPlot = {
    x: years,
    y: afforestations,
    type: 'bar',
    name: '<b>Afforestation<b>',
    marker: {color: '#17cc10'},
  };

  const deforestationPlot = {
    x: years,
    y: deforestations,
    type: 'bar',
    name: '<b>Deforestation<b>',
    marker: {color: '#F05D5E'},
  };

  const graphTitle = window.innerWidth >= 700 ?
    `<b>Forest Cover in ${country}</b>` : `<b>Forest Cover in<br>${country}</b>`;

  const description = 'This graph is meant to represent a chosen country\'s forest ' +
    'cover level, as well as the amount of afforestation and deforestation overtime. ' +
    'Countries often only provide afforestation/deforestation data every 5 or so years, ' +
    'and some are not reporting those statistics at all. The hypothesis for this ' +
    'graph was that, after years when afforestation was high, forest cover would ' +
    'gradually rise in the coming years, and fall after high levels of deforestation.';
  
  return(
    <section className="container-view">
      <Sidebar dataView={props.dataView} setCountry={setCountry} description={description}
        forestryCountries={countryList}/>
      <Plot
        data = {[
          {
            x: years,
            y: forestCovers,
            type: 'scatter',
            name: '<b>Forest cover %<b>',
            mode: 'lines+markers',
            marker: {color: '#00d9ff'},
            yaxis: 'y2'
          },
          afforestationPlot,
          deforestationPlot
        ]}
        layout = {
          {
            autosize: true,
            legend: {'orientation': 'h', 
              font: {weight: 'bold', color: '#ffffff'}},
            xaxis: {
              tickfont: {color: '#ffffff'}
            },
            title: {text: graphTitle,
              font: { weight: 500, color: '#ffffff' }},
            yaxis: {
              automargin: true,
              title: `<b>Afforestation/Deforestation</b>`,
              titlefont: { color: '#ffffff'},
              tickfont: { color: '#ffffff'},
            },
            yaxis2: {
              automargin: true,
              title: `<b>Forest Cover %</b>`,
              titlefont: { color: '#ffffff'},
              overlaying: 'y',
              side: 'right',
              tickfont: {color: '#ffffff'}
            },
            barmode: 'group',
            plot_bgcolor: '#EAF2EF',
            paper_bgcolor: '#1B512D',
          }
        }
        useResizeHandler
      />
    </section>
  );
}