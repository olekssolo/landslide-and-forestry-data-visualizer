import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAnglesRight } from '@fortawesome/free-solid-svg-icons';
import { useState } from 'react';
import { 
  SidebarFilter, SidebarDescription, SidebarContributors
} from './SidebarIcons';
import {
  LandslideFilterContent, DescriptionContent, ContributorsContent,
  ForestCountryFilterContent, DataAnalysisFilter
} from './SidebarContent';
import '../../stylesheets/Sidebar.css';


/**
 * @description A panel component in charge of managing the state of icons/options, their onclick 
 * reactions, and their layout
 * @param yearRange The state variable containing the startYear and endYear for data filtering
 * @param setYearRange The state update variable for yearRange
 * @returns The sidebar containing icons, options and sidebar data
 */
export default function Sidebar(props) {
  const [option, setOption] = useState('filter');

  const handleOptionClick = (option) => {
    setOption(option);
  };

  function clickSidebarIcon() {
    const simulatedClick = new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: true,
      clientX: 20
    });
    document.getElementById('awesome-icon').dispatchEvent(simulatedClick);
  }

  return(
    <div className="sidebar">
      <div className="icon-container">
        <div className="sidebar-icon">
          <FontAwesomeIcon icon={faAnglesRight} size="3x" id="awesome-icon" onClick={() => {
            const sidebar = document.getElementsByClassName('sidebar')[0];
            const sidebarIcon = document.getElementsByClassName('sidebar-icon')[0];
            const sidebarContent = document.getElementsByClassName('sidebar-content')[0];
            const iconContainer = document.getElementsByClassName('icon-container')[0];
            let width = '30rem';

            if (window.innerWidth < 600) {
              width = '100%';
            }
            
            if (sidebar.style.width === '0rem' || sidebar.style.width === '') {
              sidebar.style.width = width;
              sidebarIcon.style.transform = 'rotate(180deg)';
              sidebarContent.style.display = 'flex';
              if (window.innerWidth < 600) {
                iconContainer.style.marginTop = '0px';
                sidebar.style.zIndex = '2';
              }
            } else {
              sidebar.style.width = '0rem';
              sidebarIcon.style.transform = 'rotate(0deg)';
              sidebarContent.style.display = 'none';
              if (window.innerWidth < 600) {
                iconContainer.style.marginTop = 'calc(100vh - 9rem)';
                sidebar.style.zIndex = '0';
              }
            }
          }
          }/>
        </div>
        <div id="option-icons">
          <SidebarFilter option={option} clickSidebarIcon={clickSidebarIcon} 
            handleOptionClick={handleOptionClick}/>
          <SidebarDescription option={option} clickSidebarIcon={clickSidebarIcon} 
            handleOptionClick={handleOptionClick}/>
          <SidebarContributors option={option} clickSidebarIcon={clickSidebarIcon} 
            handleOptionClick={handleOptionClick}/>
        </div>
      </div>
      <div className="sidebar-content">
        <span className="header-container">
          <h1 className="sidebar-title">Information</h1>
        </span>
        <section className="sidebar-text-container">
          {option === 'filter' && props.dataView === 'landslide' ?
            <LandslideFilterContent {...props} />
            : option === 'filter' && props.dataView === 'forestcover' ?
              <ForestCountryFilterContent {...props}/>
              : option === 'filter' && props.dataView === 'analysis' ?
                <DataAnalysisFilter {...props}/>
                :
                <></>
          }
          {option === 'description' ?
            <DescriptionContent description={props.description}/>
            :
            <></>
          }
          {option === 'contributors' ?
            <ContributorsContent />
            :
            <></>
          }
        </section>
      </div>
    </div>
  );
}