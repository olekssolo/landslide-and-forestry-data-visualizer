import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLink, faCalendarDays, faArrowRightLong, 
  faGlobe } from '@fortawesome/free-solid-svg-icons';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import { useState } from 'react';

/**
 * @description Displays the information displayed when the contributors option is selected
 * @returns A list of information pertaining to contributors/attribution/libraries
 */
function ContributorsContent() {
  return(
    <div className="option-content">
      <h2>Contributors</h2>
      <h3>Developers</h3>
      <span className="option-description">
        <ul>
          <li>Christopher Kwok</li>  
          <li>Thomas Roos</li>
          <li>Oleksander Sologub</li>
        </ul>
      </span>
      <h3>Attributions</h3>
      <span className="option-description">
        <div className="link-div">
          <FontAwesomeIcon icon={faLink} size="1x" 
            onClick={() => {
              window.open('https://data.nasa.gov/Earth-Science/Global-' 
              + 'Landslide-Catalog-Export/dd9e-wu2v/data#Export');
            }}/>
          <a href={'https://data.nasa.gov/Earth-Science/Global-' 
          + 'Landslide-Catalog-Export/dd9e-wu2v/data#Export'}>
            Landslide Dataset
          </a>
        </div>
        <div className="link-div">
          <FontAwesomeIcon icon={faLink} size="1x" 
            onClick={() => {
              window.open('https://github.com/owid/owid-datasets/blob/master/datasets/'
              + 'Forest%20land%2C%20deforestation%20and%20change%20(FAO%2C%202020)/For' 
              + 'est%20land%2C%20deforestation%20and%20change%20(FAO%2C%202020).csv');
            }}/>
          <a href={'https://github.com/owid/owid-datasets/blob/master/datasets/'
              + 'Forest%20land%2C%20deforestation%20and%20change%20(FAO%2C%202020)/For' 
              + 'est%20land%2C%20deforestation%20and%20change%20(FAO%2C%202020).csv'}>
            ForestCover Dataset
          </a>
        </div>
      </span>
      <h3>Libraries</h3>
      <div className="link-div">
        <FontAwesomeIcon icon={faLink} size="1x" 
          onClick={() => {
            window.open('https://github.com/Leaflet/Leaflet.heat');
          }}/>
        <a href="https://github.com/Leaflet/Leaflet.heat">
          Leaflet-Heat
        </a>
      </div>
      <div className="link-div">
        <FontAwesomeIcon icon={faLink} size="1x" 
          onClick={() => {
            window.open('https://plotly.com/');
          }}/>
        <a href="https://plotly.com/">
          Plotly
        </a>
      </div>
      <div className="link-div">
        <FontAwesomeIcon icon={faLink} size="1x" 
          onClick={() => {
            window.open('https://mui.com/material-ui/react-slider/');
          }}/>
        <a href="https://mui.com/material-ui/react-slider/">
          React-Slider
        </a>
      </div>
    </div>
  );
}

/**
 * @description Displays the filter input options for the user to filter the data being displayed
 * @param yearRange The state variable containing the startYear and endYear for data filtering
 * @param setYearRange The state update variable for yearRange
 * @returns A JSX panel that allows the user to filter data
 */
function LandslideFilterContent(props) {
  const [localRange, setLocalRange] = useState([props.yearRange[0], props.yearRange[1]]);

  function convertInt(value) {
    return `${value}`;
  }

  const handleFilter = () => {
    if (localRange[0] !== props.yearRange[0] || localRange[1] !== props.yearRange[1]) {
      props.setYearRange([localRange[0], localRange[1]]);
    }
  };

  const minDistance = 0;

  const handleChange = (event, value, active) => {
    if (!Array.isArray(value)) {
      return;
    }

    // Update the state variable and the input values
    const startYear = document.getElementById('start-year');
    const endYear = document.getElementById('end-year');

    if (active === 0) {
      const start = Math.min(value[0], value[1] - minDistance);
      setLocalRange([start, value[1]]);
      startYear.value = start;
      endYear.value = value[1];
    } else {
      const end = Math.max(value[1], value[0] + minDistance);
      setLocalRange([value[0], end]);
      startYear.value = value[0];
      endYear.value = end;
    }
  };

  return(
    <div className="option-content">
      <h2>Year Filter</h2>
      <div className="option-description">
        <Box sx={{ width: 300 }}>
          <Slider
            getAriaLabel={() => 'Minimum distance'}
            value={localRange}
            onChange={handleChange}
            valueLabelDisplay="auto"
            getAriaValueText={convertInt}
            disableSwap
            step={1}
            marks
            min={1993}
            max={2016}
          />
        </Box>
        <div id="year-display">
          <FontAwesomeIcon icon={faCalendarDays} size="2x" />
          <input type="number" className="year-range" id="start-year" min="1993" max={localRange[1]}
            step="1" defaultValue={localRange[0]} onChange={(event) => {
              setLocalRange([parseInt(event.target.value), localRange[1]]);
            }}/>
          <FontAwesomeIcon icon={faArrowRightLong} size="2x" />
          <input type="number" className="year-range" id="end-year" min={localRange[0]} max="2017" 
            step="1" defaultValue={localRange[1]} onChange={(event) => {
              setLocalRange([localRange[0], parseInt(event.target.value)]);
            }}/>
          <FontAwesomeIcon icon={faCalendarDays} size="2x" />
        </div>
        <span className="submit-query">
          <button id="query-map" onClick={handleFilter}>Filter</button>
        </span>
      </div>
    </div>
  );
}

function DataAnalysisFilter(props){
  
  return(
    <div className="option-content">
      <h2>Country Filter</h2>
      <div className="option-description">
        <FontAwesomeIcon icon={faGlobe} size="2x" />
        <select id="country-selector" onChange={(event) => props.setCountry(event.target.value)}>
          {props.commonCountries.map((country) => {
            if (country === 'Canada') {
              return <option key={country} selected>{country}</option>;
            } else {
              return <option key={country}>{country}</option>;
            }
          })}
        </select>
      </div>
    </div>
  );
}

/**
 * Allows the user to select what country to show forest cover data for
 * @param {*} setCountry the state function that changes current country
 * @returns a JSX panel that allows the user to select the country
 */
function ForestCountryFilterContent(props) {
  return (
    <>
      <div className="option-content">
        <h2>Country Filter</h2>
        <div className="option-description">
          <FontAwesomeIcon icon={faGlobe} size="2x" />
          <select name="countries" id="country-select" 
            onChange={(e) => props.setCountry(e.target.value)}>
            {props.forestryCountries.map((country) => {
              if (country === 'Canada') {
                return <option key={country} selected>{country}</option>;
              } else {
                return <option key={country}>{country}</option>;
              }
            })}
          </select>
        </div>
      </div>
    </>
  );
}

/**
 * @description Displays the information displayed when the description option is selected
 * @returns A description of the currently selected view
 */
function DescriptionContent(props) {
  
  return(
    <div className="option-content">
      <h2>Description</h2>
      <span className="option-description">
        {props.description}
      </span>
    </div>
  );
  
}

export {
  LandslideFilterContent,
  DataAnalysisFilter,
  DescriptionContent,
  ContributorsContent,
  ForestCountryFilterContent
};
