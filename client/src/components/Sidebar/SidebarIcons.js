import { faPeopleGroup, faFilter, faCircleInfo } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * @description The contributors option icons - manages the state of the icon
 * @param option The user's current option of the sidebar options
 * @param clickSidebarIcon A simulation event that activates the opening/closing of the sidebar 
 * @param handleOptionClick A function that handles and sets the state option
 * @returns An icon with the appropriate onClick events and styling elements
 */
function SidebarContributors(props) {
  return(
    <>
      {props.option === 'contributors' ?
        <div className="option-icon-selected" id="selected-contributor" 
          onClick={props.clickSidebarIcon}>
          <span className="tooltip-1"><b>Contributors</b></span>
          <FontAwesomeIcon icon={faPeopleGroup} size="2x"/>
        </div>
        :
        <div className="option-icon-unselected" id="unselected-contributor" onClick={() => {
          const sidebar = document.getElementsByClassName('sidebar')[0];
          props.handleOptionClick('contributors');
          if (sidebar.style.width === '5rem' || sidebar.style.width === '') {
            props.clickSidebarIcon();
          }
        }}>
          <span className="tooltip-1"><b>Contributors</b></span>
          <FontAwesomeIcon icon={faPeopleGroup} size="2x"/>
        </div>
      }
    </>
  );
}

/**
 * @description The filter option icons - manages the state of the icon
 * @param option The user's current option of the sidebar options
 * @param clickSidebarIcon A simulation event that activates the opening/closing of the sidebar 
 * @param handleOptionClick A function that handles and sets the state option
 * @returns An icon with the appropriate onClick events and styling elements
 */
function SidebarFilter({ option, clickSidebarIcon, handleOptionClick }) {
  return(
    <>
      {option === 'filter' || option === 'none' ?
        <div className="option-icon-selected" id="selected-filter" onClick={clickSidebarIcon}>
          <span className="tooltip-3"><b>Filter</b></span>
          <FontAwesomeIcon icon={faFilter} size="2x"/>
        </div>
        :
        <div className="option-icon-unselected" id="unselected-filter" onClick={() => {
          const sidebar = document.getElementsByClassName('sidebar')[0];
          handleOptionClick('filter');
          if (sidebar.style.width === '5rem' || sidebar.style.width === '') {
            clickSidebarIcon();
          }
        }}>
          <span className="tooltip-3"><b>Filter</b></span>
          <FontAwesomeIcon icon={faFilter} size="2x"/>
        </div>
      }
    </>
  );
}

/**
 * @description The descroption option icons - manages the state of the icon
 * @param option The user's current option of the sidebar options
 * @param clickSidebarIcon A simulation event that activates the opening/closing of the sidebar 
 * @param handleOptionClick A function that handles and sets the state option
 * @returns An icon with the appropriate onClick events and styling elements
 */
function SidebarDescription({ option, clickSidebarIcon, handleOptionClick }) {
  return(
    <>
      {option === 'description' ?
        <div className="option-icon-selected" id="selected-description" onClick={clickSidebarIcon}>
          <span className="tooltip-4"><b>Description</b></span>
          <FontAwesomeIcon icon={faCircleInfo} size="2x"/>
        </div>
        :
        <div className="option-icon-unselected" id="unselected-description" onClick={() => {
          const sidebar = document.getElementsByClassName('sidebar')[0];
          handleOptionClick('description');
          if (sidebar.style.width === '5rem' || sidebar.style.width === '') {
            clickSidebarIcon();
          }
        }}>
          <span className="tooltip-4"><b>Description</b></span>
          <FontAwesomeIcon icon={faCircleInfo} size="2x"/>
        </div>
      }
    </>
  );
}

export {
  SidebarContributors,
  SidebarFilter,
  SidebarDescription
};