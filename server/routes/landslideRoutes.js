const express = require('express');
const DB = require('../db/db.js');

const router = express.Router();
const db = new DB();

/**
 * @swagger
 * /landslide/country/{country}:
 *   get:
 *     summary: Retrieve a list of landslide data for a specific country
 *     description: Retrieve a landslide data from a specific country, with an optional year query
 *     parameters:
 *       - in: path
 *         name: country
 *         required: true
 *         description: String representation of a valid country to retrieve
 *         schema:
 *           type: string
 *       - in: query
 *         name: year
 *         required: false
 *         description: Integer that states the data year to retrieve
 *         schema:
 *           type:
 *             type: integer
 *     responses:
 *       200:
 *         description: A list of landslide data
 *         content: 
 *           application/json:
 *             schema: 
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: Landslide id
 *                     example: 5844
 *                   country: 
 *                     type: string
 *                     description: A valid country
 *                     example: Canada
 *                   year: 
 *                     type: integer
 *                     description: A year in the Gregorian calendar
 *                     example: 2000
 *                   category:
 *                     type: string
 *                     description: The type of landslide
 *                     example: rock_fall
 *                   trigger: 
 *                     type: string
 *                     description: The cause of the landslide
 *                     example: downpour
 *                   size: 
 *                     type: string
 *                     description: The size of the landslide
 *                     example: small
 *                   fatality: 
 *                     type: integer
 *                     description: The number of deaths caused by the landslide
 *                     example: 5
 *                   coordinates: 
 *                     type: array
 *                     description: The coordinates locating the landslide
 *                     example: [6.5662,45.8358]
 *       400:
 *         description: The user provided invalid data
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 400
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: Year must be a valid integer greater than 0 and less than 2017
 *       500:
 *         description: There was an internal server error
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 500
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: Server failed to query the database
 */
router.get('/country/:country', async (req, res) => {
  const selectedCountry = req.params.country;
  
  // Validate country
  if (typeof selectedCountry !== 'string') {
    res.status(400).json(
      { 
        status : 400, 
        error : 'Country must be a valid string'
      }
    );
    return 0;
  }

  // Get a list of all the unique country names
  let landslideCountries = [];
  try {
    landslideCountries = await db.landslideGetUniqueValues('country');
  } catch {
    res.status(500).json(
      { 
        status : 500, 
        error : 'Server failed to query the database'
      }
    );
    return 0;
  }

  // Verify that the country is a proper country
  if (!landslideCountries.includes(selectedCountry)) {
    res.status(400).json(
      { 
        status : 400, 
        error : 'Country must be a valid country contained within the database'
      }
    );
    return 0;
  }

  // Get query parameter
  let year = null;
  if (req.query.year !== null && req.query.year !== undefined) {
    year = parseInt(req.query.year);
    if (req.query.year.match(/^[0-9]+$/) === null) {
      res.status(400).json(
        { 
          status : 400, 
          error : `Year must be a valid integer greater than 0 and less than 2017`
        }
      );
      return 0;
    } else if (year > 2017 || year < 0) {
      res.status(400).json(
        { 
          status : 400, 
          error : `Year must be a valid integer greater than 0 and less than 2017`
        }
      );
      return 0;
    }
  }

  // Query the database
  try {
    let filter = {};
    if (year !== null && year !== undefined) {
      filter = { country: selectedCountry, year : year };
    } else {
      filter = { country: selectedCountry };
    }
    const data = await db.landslideReadFiltered(filter);
    if (data.length === 0) {
      res.json({ message : '0 query results', data : [] });
    } else {
      res.send(data);
    }
  } catch {
    res.status(500).json(
      { 
        status : 500, 
        error : 'Server failed to query the database'
      }
    );
  }
}); 

/**
 * @swagger
 * /landslide/year/{year}:
 *   get:
 *     summary: Retrieve a list of landslide data for a specific year
 *     description: Retrieve a landslide data from a specific year, with an optional endyear query
 *     parameters:
 *       - in: path
 *         name: year
 *         required: true
 *         description: Integer that represents the year data to retrieve
 *         schema:
 *           type: integer
 *       - in: query
 *         name: endYear
 *         required: false
 *         description: Integer that states the upper range of data to retrieve
 *         schema:
 *           type:
 *             type: integer
 *     responses:
 *       200:
 *         description: A list of landslide data
 *         content: 
 *           application/json:
 *             schema: 
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: Landslide id
 *                     example: 5844
 *                   country: 
 *                     type: string
 *                     description: A valid country
 *                     example: Canada
 *                   year: 
 *                     type: integer
 *                     description: A year in the Gregorian calendar
 *                     example: 2000
 *                   category:
 *                     type: string
 *                     description: The type of landslide
 *                     example: rock_fall
 *                   trigger: 
 *                     type: string
 *                     description: The cause of the landslide
 *                     example: downpour
 *                   size: 
 *                     type: string
 *                     description: The size of the landslide
 *                     example: small
 *                   fatality: 
 *                     type: integer
 *                     description: The number of deaths caused by the landslide
 *                     example: 5
 *                   coordinates: 
 *                     type: array
 *                     description: The coordinates locating the landslide
 *                     example: [6.5662,45.8358]
 *       400:
 *         description: The user provided invalid data
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 400
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: Year must be a valid integer greater than 0 and less than 2017
 *       500:
 *         description: There was an internal server error
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 500
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: Server failed to query the database
 */
router.get('/year/:year', async (req, res) => {
  const year = parseInt(req.params.year);
  let endYear = parseInt(req.query.endYear);

  // Validate year
  if (req.params.year.match(/^[0-9]+$/) === null || year >= 2017) {
    res.status(400).json(
      { 
        status : 400, 
        error : `Year must be a valid integer greater than 0 and less than 2017`
      }
    );
    return 0;
  }

  if (req.query.endYear !== null && req.query.endYear !== undefined) {
    if (req.query.endYear.match(/^[0-9]+$/) === null || endYear < year) {
      res.status(400).json(
        { 
          status : 400, 
          error : `EndYear must be a valid integer greater than or equal to ${year}`
          + ` and less than 2017`
        }
      );
      return 0;
    }
  } else {
    endYear = year;
  }

  // Query the database
  try {
    const lowerBound = year - 1;
    const uppderBound = endYear + 1;
    const filter = { year : { $gt : lowerBound, $lt : uppderBound } };
    const data = await db.landslideReadFiltered(filter);
    if (data.length === 0) {
      res.json({ message : '0 query results', data : [] });
    } else {
      res.send(data);
    }
  } catch {
    res.status(500).json(
      { 
        status : 500, 
        error : 'Server failed to query the database'
      }
    );
  }
});

module.exports = router;