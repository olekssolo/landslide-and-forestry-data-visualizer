const express = require('express');
const DB = require('../db/db.js');

const router = express.Router();
const db = new DB();

/**
 * @swagger
 * /country/landslide/:
 *   get:
 *     summary: Retrieve a list of all valid landslide countries
 *     description: Retrieve a list of all valid countries within the landslide dataset
 *     responses:
 *       200:
 *         description: A list of countries
 *         content: 
 *           application/json:
 *             schema: 
 *               type: array
 *               description: A list of countries
 *               example: ['Canada', 'France', 'Mexico']
 *       500:
 *         description: Internal server error
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 500
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: Server failed to query the database
 */
router.get('/landslide', async (req, res) => {
  try {
    const landslideCountries = await db.landslideGetUniqueValues('country');
    res.send(landslideCountries);
  } catch(err) {
    res.status(500).json(
      { 
        status : 500, 
        error : 'Server failed to query the database'
      }
    );
  }
});

/**
 * @swagger
 * /country/forestry/:
 *   get:
 *     summary: Retrieve a list of all valid forestry countries
 *     description: Retrieve a list of all valid countries within the forestry dataset
 *     responses:
 *       200:
 *         description: A list of countries
 *         content: 
 *           application/json:
 *             schema: 
 *               type: array
 *               description: A list of countries
 *               example: ['Canada', 'France', 'Mexico']
 *       500:
 *         description: Internal server error
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 500
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: Server failed to query the database
 */
router.get('/forestry', async (req, res) => {
  try {
    const landslideCountries = await db.forestryGetUniqueValues('Country');
    res.send(landslideCountries);
  } catch(err) {
    res.status(500).json(
      { 
        status : 500, 
        error : 'Server failed to query the database'
      }
    );
  }
});

/**
 * @swagger
 * /country/common/:
 *   get:
 *     summary: Retrieve a list of all valid common countries
 *     description: Retrieve a list of all valid countries common to both datasets
 *     responses:
 *       200:
 *         description: A list of countries
 *         content: 
 *           application/json:
 *             schema: 
 *               type: array
 *               description: A list of countries
 *               example: ['Canada', 'France', 'Mexico']
 *       500:
 *         description: Internal server error
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 500
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: Server failed to query the database
 */
router.get('/common', async (req, res) => {
  try {
    const landslideCountries = await db.landslideGetUniqueValues('country');
    const forestryCountries = await db.forestryGetUniqueValues('Country');
    const commonCountries = landslideCountries.filter( value => 
      forestryCountries.includes(value));
    res.send(commonCountries);
  } catch(err) {
    res.status(500).json(
      { 
        status : 500, 
        error : 'Server failed to query the database'
      }
    );
  }
});

module.exports = router;