const express = require('express');
const DB = require('../db/db.js');

const forestryRouter = express.Router();
const db = new DB();

forestryRouter.use(express.json());

/**
 * @swagger
 * /forestry/country/{country}:
 *   get:
 *     summary: Retrieve a list of forestry data for a specific country
 *     description: Retrieve a forestry data from a specific country, with an optional year query
 *     parameters:
 *       - in: path
 *         name: country
 *         required: true
 *         description: String representation of a valid country to retrieve
 *         schema:
 *           type: string
 *       - in: query
 *         name: year
 *         required: false
 *         description: Integer that states the data year to retrieve
 *         schema:
 *           type:
 *             type: integer
 *     responses:
 *       200:
 *         description: A list of forestry data
 *         content: 
 *           application/json:
 *             schema: 
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   Country:
 *                     type: string
 *                     description: A valid country
 *                     example: Canada
 *                   Year: 
 *                     type: integer
 *                     description: A year in the Gregorian Calendar
 *                     example: 2000
 *                   Afforestation: 
 *                     type: integer
 *                     description: Afforestation statistic
 *                     example: 2.47
 *                   Deforestation:
 *                     type: integer
 *                     description: Deforestation statistic
 *                     example: 37.52
 *                   ForestCover: 
 *                     type: integer
 *                     description: Forestcover percentage
 *                     example: 38.84025858867068
 *                   ForestArea: 
 *                     type: integer
 *                     description: Forestarea percentage
 *                     example: 348084.54600000003
 *       400:
 *         description: The user provided invalid data
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 400
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: The year param must not contain characters
 *       500:
 *         description: There was an internal server error
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 500
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: Failed to query the database
 */
forestryRouter.get('/country/:country', async (req, res) => {
  // get array of valid countries in the collection
  let forestryCountries;
  try {
    forestryCountries = await db.forestryGetUniqueValues('Country');
  } catch (error) {
    res.status(500).json({
      status: 500,
      error: 'Failed to query the database'
    });
  }

  const countryChoice = req.params.country;
  let yearQuery = null;
  // null/undefined check
  if (req.query.year !== null && req.query.year !== undefined) {
    // now check year query for characters (e.g. 2008abc, 2abc003)
    if (req.query.year.search(/^[0-9]+$/) === -1) {
      res.status(400).json({
        status: 400,
        error: 'The year param must not contain characters'
      });

      return 0;
    } else {
      yearQuery = parseInt(req.query.year);
    }
  }
  // choose filter depending on presence/absence of year query
  const filter = yearQuery === null ?
    {Country: countryChoice} : {Country: countryChoice, Year: yearQuery};
  
  // validate string
  if (typeof countryChoice !== 'string') {
    res.status(400).json({
      status: 400,
      error: 'Country must be a valid string'
    });

    return 0;
  }
  if (!forestryCountries.includes(countryChoice)) {
    res.status(400).json({
      status: 400,
      error: 'Country must be a valid country contained within the database'
    });

    return 0;
  }
  // year query param validation, error messages describe what
  if (yearQuery !== null) {
    if (isNaN(yearQuery)) {
      res.status(400).json({
        status: 400,
        error: 'Year must be a valid integer greater than 0'
      });

      return 0;
    }
    if (yearQuery > 2020) {
      res.status(400).json({
        status: 400,
        error: 'This API only contains data up to the year 2020'
      });

      return 0;
    }
  }
  // finally connect to db and get data using filter
  try {
    const countryData = await db.forestryReadFiltered(filter);
    // lets users know clearly that there was no data found
    if (countryData.length === 0) {
      res.json({
        message: 'No data found',
        data: countryData
      });
    } else {
      res.status(200).send(countryData);
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({
      status: 500,
      error: error.message
    });
  }
});

/**
 * @swagger
 * /forestry/year/{year}:
 *   get:
 *     summary: Retrieve the forestry data for a particular year
 *     description: Retrieve forestry data for a particular year with an optional endyear query
 *     parameters:
 *       - in: path
 *         name: year
 *         required: true
 *         description: Integer that states the data year to retrieve
 *         schema:
 *           type: integer
 *       - in: query
 *         name: endYear
 *         required: false
 *         description: Integer that states the upper range of data to retrieve
 *         schema:
 *           type:
 *             type: integer
 *     responses:
 *       200:
 *         description: A list of forestry data
 *         content: 
 *           application/json:
 *             schema: 
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   Country:
 *                     type: string
 *                     description: A valid country
 *                     example: Canada
 *                   Year: 
 *                     type: integer
 *                     description: A year in the Gregorian Calendar
 *                     example: 2000
 *                   Afforestation: 
 *                     type: integer
 *                     description: Afforestation statistic
 *                     example: 2.47
 *                   Deforestation:
 *                     type: integer
 *                     description: Deforestation statistic
 *                     example: 37.52
 *                   ForestCover: 
 *                     type: integer
 *                     description: Forestcover percentage
 *                     example: 38.84025858867068
 *                   ForestArea: 
 *                     type: integer
 *                     description: Forestarea percentage
 *                     example: 348084.54600000003
 *       400:
 *         description: The user provided invalid data
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 400
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: Year must be a valid integer greater than 0
 *       500:
 *         description: There was an internal server error
 *         content: 
 *           application/json:
 *             schema: 
 *               type: object
 *               properties:
 *                 status:
 *                   type: integer
 *                   description: Error status code
 *                   example: 500
 *                 error:
 *                   type: string
 *                   description: Error message
 *                   example: Internal server error
 */
forestryRouter.get('/year/:year', async (req, res) => {
  const yearChoice = parseInt(req.params.year);
  const endYear = req.query.endYear;

  if (endYear !== undefined && endYear !== null) {
    if (endYear.search(/^[0-9]+$/) === -1) {
      res.status(400).json({
        status: 400,
        error: 'The endYear query param must not contain characters'
      });

      return 0;
    }
  }

  // set the filter depending on presence/absense of yearEnd query
  const filter = endYear === undefined ?
    {Year: yearChoice} : {Year: { $gt: yearChoice - 1, $lt: parseInt(endYear) + 1 }};
  
  // year choice validation, error messages describe what
  if (yearChoice !== undefined) {
    if (isNaN(yearChoice)) {
      res.status(400).json({
        status: 400,
        error: 'Year must be a valid integer greater than 0'
      });

      return 0;
    }
    if (yearChoice > 2020 || parseInt(endYear) > 2020) {
      res.status(400).json({
        status: 400,
        error: 'This API only contains data up to the year 2020'
      });

      return 0;
    } 
    if (yearChoice >= parseInt(endYear)) {
      res.status(400).json({
        status: 400,
        error: 'The year parameter should not be greater than the endYear parameter; '
        + 'endYear represents the limit of the year range.'
      });

      return 0;
    }
    if (req.params.year.search(/^[0-9]+$/) === -1) {
      res.status(400).json({
        status: 400,
        error: 'The year param must not contain characters'
      });

      return 0;
    }
  }

  // finally connect to db and get data using filter
  try {
    const yearData = await db.forestryReadFiltered(filter);
    if (yearData.length === 0) {
      res.json({
        message: 'No data found',
        data: yearData
      });
    } else {
      res.status(200).send(yearData);
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({
      status: 500,
      error: error.message
    });
  }
});

module.exports = forestryRouter;