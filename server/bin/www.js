#!/usr/bin/env node 
const app = require('../api.js'); 
const DB = require('../db/db.js');
const port = process.env.PORT || 3000;

(async () => {
  try {
    const db = new DB();
    await db.connect('cluster0', 'landslideData');
  } catch (e) {
    console.error('could not connect');
    console.dir(e);
    process.exit();
  }
  app.listen(port, () => {
    console.log(`Server listening on port ${port}!`);
  });
})();
