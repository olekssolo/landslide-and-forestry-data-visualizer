const DB = require('../db/db.js');
const fs = require('fs/promises');

/**
 * This IIFE creates a .json file with countries that are in the forestry CSV
 * file. This is used for the sidebar component on the client side, to display
 * the correct countries for the Forestry graph.
 */
async function getForestryCountries(){
  let db;
  try {
    db = new DB();
    await db.connect('cluster0', 'forestryData');
    const uniqueCountriesForestry = await db.getUniqueValues('Country');
    console.log(uniqueCountriesForestry);

    await fs.writeFile('./data/forestryCountries.json', 
      JSON.stringify(uniqueCountriesForestry, null, 4), 'utf8');
  } catch(e) {
    console.error(e.message);
  } finally {
    process.exit();
  }
}

(async () => {
  await getForestryCountries();
})();