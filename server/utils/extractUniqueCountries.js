const DB = require('../db/db.js');
const fs = require('fs/promises');

async function getCountries(){
  let db;
  try {
    db = new DB();
    await db.connect('cluster0', 'forestryData');
    const uniqueCountriesForestry = await db.getUniqueValues('Country');
    console.log(uniqueCountriesForestry);
    await db.changeCollection('landslideData');
    const uniqueCountriesLandslide = await db.getUniqueValues('country');
    console.log(uniqueCountriesLandslide);

    const commonCountries = uniqueCountriesForestry.filter(value => 
      uniqueCountriesLandslide.includes(value));
    await fs.writeFile('./data/commonCountries.json', 
      JSON.stringify(commonCountries, null, 4), 'utf8');
  } catch(e) {
    console.error(e.message);
  } finally {
    process.exit();
  }
}

(async () => {
  await getCountries();
})();