const readFile = require('./fileReader.js');

/** Helper method that filters the necessary columns from landslide json and
 * creates an object that can be added to a JSON
 * 
 * @param {Array} arr The array that needs to be filtered
 * @returns An object with the required values from the array
 */
function filterLandslideArray(arr) {
  try {
    // Data validation
    const id = parseInt(arr[27]);
    const country = arr[28];
    const year = parseInt(arr[11].slice(0, 4));
    const category = arr[17];
    const trigger = arr[18];
    const size = arr[19];
    const fatality = Number(arr[21]);
    const longitude = parseFloat(arr[37]);
    const latitude = parseFloat(arr[38]);

    // Conditional checker
    if (isNaN(id)) {
      return;
    } else if (typeof country !== 'string') {
      return;
    } else if (isNaN(year) || year < 1970 || year > new Date().getFullYear()) {
      return;
    } else if (typeof category !== 'string') {
      return;
    } else if (typeof trigger !== 'string') {
      return;
    } else if (typeof size !== 'string') {
      return;
    } else if (isNaN(fatality)) {
      return;
    } else if (isNaN(longitude)) {
      return;
    } else if (isNaN(latitude)) {
      return;
    }

    // Set the data object
    const parsedObject = {
      'id' : id,
      'country' : country,
      'year' : year,
      'category' : category,
      'trigger' : trigger,
      'size' : size,
      'fatality' : fatality,
      'coordinates' : [longitude, latitude]
    };

    return parsedObject;
  } catch {
    return;
  }
}

/** Goes through the landslide json file and creates an array of objects with
 * all the unnecessary data filtered out
 * 
 * @param {string} path The path of the file to filter
 * @returns {Array} An array of objects with necessary data
 */
async function parseLandslideJson(path) {
  const data = await readFile(path);
  const json = JSON.parse(data);
  const jsonData = [];

  for (const landslide of json.data) {
    const dataRow = filterLandslideArray(landslide);
    if (dataRow !== null && dataRow !== undefined) {
      jsonData.push(dataRow);
    }
  }
  
  return jsonData;
}

module.exports = parseLandslideJson;