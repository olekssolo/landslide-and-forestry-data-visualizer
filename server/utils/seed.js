const DB = require('../db/db.js');
const parseLandslideJson = require('./landslideDataParser.js');
const parseForestData = require('./forestDataParser.js');

/**
 * seedDB is a method that is used to seed the database with parsed data using
 * @return {Array} number of rows of data inserted for each insertMany call.
 */
async function seedDB () {
  let db;
  const insertedRows = [];
  try {
    // Read & parse the landslide/forestry data
    const landslideData = await parseLandslideJson('./data/landslide_data.json');
    const forestryData = await parseForestData();

    // Create a database connection and seed the data
    const db = new DB();
    await db.connect('cluster0', 'landslideData');
    
    if (await db.getCollectionCount('landslideData') === 0) {
      insertedRows[0] = await db.landslideCreateMany(landslideData);
      console.log(`Inserted ${insertedRows[0].insertedCount} pieces of Landslide Data`);
    } else {
      insertedRows[0] = 0;
      console.log(`Landslide data already found in the database`);
    }

    if (await db.getCollectionCount('forestryData') === 0) {
      insertedRows[1] = await db.forestryCreateMany(forestryData);
      console.log(`Inserted ${insertedRows[1].insertedCount} pieces of Forestry Data`);
    } else {
      insertedRows[1] = 0;
      console.log(`Forestry data already found in the database`);
    }

  } catch (e) {
    console.error('Unable to seed database');
    console.dir(e);
  } finally {
    if (db) {
      db.close();
    }
  }
  return insertedRows;
}

/** Removes any undefineds in an array
 * @param {Array} dataArray 
 * leaving this function here for later use,
 * remove before final submission if unused
function stripUndefined(dataArray) {
  const filteredArray = dataArray.filter(function(element) {
    return element !== undefined;
  });
  return filteredArray;
}
*/

module.exports = seedDB;