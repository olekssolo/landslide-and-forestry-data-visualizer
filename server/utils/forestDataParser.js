const readFile = require('./fileReader.js');

/** translateForestCSVToJson takes in the data from the file as a string
 * then translates it into a json object, filtering to only include the data
 * relevant to the project.
 * 
 * @param {string} path Path to file to parse, default: forestry CSV
 * @returns {} json array representing the filtered forestry data.
 */
async function translateForestCSVToJson(
  path = './data/Forest land, deforestation and change (FAO, 2020).csv'
) {
  const data = await readFile(path);
  const translatedRows = [];
  const lines = data.split('\n');
  for (var i = 1; i < lines.length; i++) {
    const currentLine = lines[i];
    const columns = currentLine.split(',');
    const filteredDataRow = filterRow(columns);
    if (filteredDataRow !== undefined) {
      translatedRows.push(filterRow(columns));
    }
  }
  return translatedRows;
}

/** Takes a data row and filters the data to include the columns that we want.
 *  Also validates that the strings have properly been converted to numbers.
 * 
 * @param {Array} dataRow represents an entity's forestry data
 * @returns filtered object containing the filtered data
 */
function filterRow(dataRow){

  const country = dataRow[0];
  const year = parseInt(dataRow[1]);
  let afforestation = parseFloat(dataRow[2]);
  let deforestation = parseFloat(dataRow[4]);
  let forestArea = parseFloat(dataRow[7]);
  let forestCover = parseFloat(dataRow[16]);

  if (typeof country !== 'string') {
    return;
  }
  if (typeof year !== 'number' || isNaN(year)) {
    return;
  }
  // If these are NaN it is because they are empty string and should be 0
  if (isNaN(afforestation)) {
    afforestation = 0;
  }
  if (isNaN(deforestation)) {
    deforestation = 0;
  }
  if (isNaN(forestArea)) {
    forestArea = 0;
  }
  if (isNaN(forestCover)) {
    forestCover = 0;
  }
  
  return {
    'Country': country,
    'Year' : year,
    'Afforestation' : afforestation,
    'Deforestation' : deforestation,
    'ForestCover' : forestCover,
    'ForestArea' : forestArea
  };
  
}

module.exports = translateForestCSVToJson;