const seed = require('./seed');

(async () => {
  await seed();
  process.exit(0);
})();