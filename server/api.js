const express = require('express');
const forestryRouter = require('./routes/forestryRouter.js');
const landslideRouter = require('./routes/landslideRoutes.js');
const countriesRouter = require('./routes/countryRouter.js');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const compression = require('compression');

// Swagger Setup
const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'Express API for 520-Final-Project',
    version: '1.0.0',
  },
};

const options = {
  swaggerDefinition,
  apis: ['./routes/*.js', './server/routes/*.js'],
};

const swaggerSpec = swaggerJSDoc(options);

const app = express();

app.use(compression());
app.use(express.static('../client/build'));

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/forestry', forestryRouter);
app.use('/landslides', landslideRouter);
app.use('/countries', countriesRouter);

module.exports = app;