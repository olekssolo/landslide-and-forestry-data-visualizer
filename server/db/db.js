require('dotenv').config();
const dbUrl = process.env.ATLAS_URI;
const {MongoClient} = require('mongodb');

let instance = null;

/**
 * Singleton that lets us access our database
 */
class DB {
  constructor() {
    if (!instance) {
      instance = this;
      this.client = new MongoClient(dbUrl);
      this.db = null;
      this.collection = null;
      this.forestryCollection = null;
      this.landslideCollection = null;
    }
    return instance;
  }

  /**
   * Connects to a collection in a database.
   * @param {String} dbName - Name of the database to connect to
   * @param {String} collName - Name of the collection 
   */
  async connect(dbName, collName) {
    if (instance.db) {
      return;
    }
    await instance.client.connect();
    instance.db = await instance.client.db(dbName);
    console.log(`Successfully Connected to MongoDB database ${dbName}`);
    instance.collection = await instance.db.collection(collName);
    instance.forestryCollection = await instance.db.collection('forestryData');
    instance.landslideCollection = await instance.db.collection('landslideData');
  }

  /**
   * Changes the collection that the instance is referring to.
   * @param {string} collName 
   */
  async changeCollection(collName) {
    instance.collection = await instance.db.collection(collName);
  }

  /**
   * Retrieves the count of records in a collection
   * @return {number} - Number of records in the collection
   */
  async getCollectionCount(collName) {
    return await instance.db.collection(collName).count();
  }

  /**
   * Closes the connection to the database.
   */
  async close() {
    await instance.client.close();
    instance = null;
  }

  /**
   * Reads all the data from the current instance.collection
   */
  async readAll() {
    return await instance.collection.find().toArray();
  }

  /** Reads from the current instance.collection after applying filters.
   * 
   * @param {Object} filters each key:value pair is a search filter
   * @returns filtered array 
   */
  async readFiltered(filters) {
    return await instance.collection.find(filters).toArray();
  }

  /** 
   * Adds once document to the database in the current collection
   * @param {Object} value - Single document to add to the database. 
   * @returns 
   */
  async create(value) {
    return await instance.collection.insertOne(value);
  }
  
  /** 
   * Adds many documents to the database in the current collection
   * @param {Array} values - An array of objects to be added to the database. 
   * @returns 
   */
  async createMany(values) {
    return await instance.collection.insertMany(values);
  }

  /**
   * Retrieves all the unique fields of a collection
   * @param {String} key - Key of the field to retrieve unique values of
   * @returns 
   */
  async getUniqueValues(key) {
    return await instance.collection.distinct(key);
  }

  // New collection specific methods

  /** Reads from the forestry collection and returns an array of filtered data
   * 
   * @param {Object} filters each key:value pair is a search filter
   * @returns {Array} filtered array from the forestry collection
   */
  async forestryReadFiltered(filters) {
    return await instance.forestryCollection.find(filters).toArray();
  }
  
  /** 
   * Adds many documents to the database in the current collection
   * @param {Array} values - An array of objects to be added to the database. 
   * @returns 
   */
  async forestryCreateMany(values) {
    return await instance.forestryCollection.insertMany(values);
  }

  /**
   * Retrieves all the unique fields of the forestry collection
   * @param {String} key - Key of the field to retrieve unique values of
   * @returns 
   */
  async forestryGetUniqueValues(key) {
    return await instance.forestryCollection.distinct(key);
  }

  /** Reads from the landslide collection and returns an array of filtered data
   * 
   * @param {Object} filters each key:value pair is a search filter
   * @returns {Array} filtered array 
   */
  async landslideReadFiltered(filters) {
    return await instance.landslideCollection.find(filters).toArray();
  }

  /** 
   * Adds many documents to the database in the current collection
   * @param {Array} values - An array of objects to be added to the database. 
   * @returns 
   */
  async landslideCreateMany(values) {
    return await instance.landslideCollection.insertMany(values);
  }

  /**
   * Retrieves all the unique fields of the forestry collection
   * @param {String} key - Key of the field to retrieve unique values of
   * @returns 
   */
  async landslideGetUniqueValues(key) {
    return await instance.landslideCollection.distinct(key);
  }


}

module.exports = DB;