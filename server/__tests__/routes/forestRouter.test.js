const request = require('supertest');
const app = require('../../api.js');
const DB = require('../../db/db.js');

jest.mock('../../db/db');

describe('GET /country/:country?year', () => {
  test('Test Success: Valid country name', async () => {
    jest.spyOn(DB.prototype, 'forestryGetUniqueValues').mockResolvedValue(
      countryNameList
    );
    jest.spyOn(DB.prototype, 'forestryReadFiltered').mockResolvedValue(
      expectedDataCanada
    );
    const response = await request(app).get('/forestry/country/Canada');
    
    expect(response.body).toEqual(expectedDataCanada);

    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Test Success: Valid country and year', async () => {
    jest.spyOn(DB.prototype, 'forestryGetUniqueValues').mockResolvedValue(
      countryNameList
    );
    jest.spyOn(DB.prototype, 'forestryReadFiltered').mockResolvedValue(
      expectedDataSameYear
    );
    const response = await request(app).get('/forestry/country/Ukraine?year=2004');
    
    expect(response.body).toEqual(expectedDataSameYear);
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });
  
  test('Test Error: Year query over 2020', async () => {
    jest.spyOn(DB.prototype, 'forestryGetUniqueValues').mockResolvedValue(
      countryNameList
    );
    jest.spyOn(DB.prototype, 'forestryReadFiltered').mockResolvedValue({
      status: 400,
      error: 'This API only contains data up to the year 2020'
    });
    const response = await request(app).get('/forestry/country/Canada?year=2022');
    
    expect(response.body).toEqual({
      status: 400,
      error: 'This API only contains data up to the year 2020'
    });
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error: Invalid country param', async () => {
    jest.spyOn(DB.prototype, 'forestryGetUniqueValues').mockResolvedValue(
      countryNameList
    );
    jest.spyOn(DB.prototype, 'forestryReadFiltered').mockResolvedValue({
      status: 400,
      error: 'Country must be a valid country contained within the database'
    });
    const response = await request(app).get('/forestry/country/ComputerScience');

    expect(response.body).toEqual({
      status: 400,
      error: 'Country must be a valid country contained within the database'
    });
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error: Year query has characters', async () => {
    jest.spyOn(DB.prototype, 'forestryGetUniqueValues').mockResolvedValue(
      countryNameList
    );
    jest.spyOn(DB.prototype, 'forestryReadFiltered').mockResolvedValue({
      status: 400,
      error: 'The year param must not contain characters'
    });

    const response = await request(app).get('/forestry/country/Canada?year=2003abc');
    expect(response.body).toEqual({
      status: 400,
      error: 'The year param must not contain characters'
    });
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });
});

describe('GET /year/:year?endYear', () => {
  test('Test Success: Valid year', async () => {
    jest.spyOn(DB.prototype, 'forestryReadFiltered').mockResolvedValue(
      expectedDataSameYear
    );
    const response = await request(app).get('/forestry/year/2003');
    
    expect(response.body).toEqual(expectedDataSameYear);
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Test Success: Valid year and endYear', async () => {
    jest.spyOn(DB.prototype, 'forestryReadFiltered').mockResolvedValue(
      expectedDataYearRange
    );
    const response = await request(app).get('/forestry/year/2003?endYear=2006');
    
    expect(response.body).toEqual(expectedDataYearRange);
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error: Year is not an integer', async () => {
    jest.spyOn(DB.prototype, 'forestryReadFiltered').mockResolvedValue({
      status: 400,
      error: 'Year must be a valid integer greater than 0'
    });
    const response = await request(app).get('/forestry/year/hello');
    
    expect(response.body).toEqual({
      status: 400,
      error: 'Year must be a valid integer greater than 0'
    });
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error: endYear lesser than year', async () => {
    jest.spyOn(DB.prototype, 'forestryReadFiltered').mockResolvedValue({
      status: 400,
      error: 'The year parameter should not be greater than the endYear parameter; '
        + 'endYear represents the limit of the year range.'
    });
    const response = await request(app).get('/forestry/year/2003?endYear=2001');
    
    expect(response.body).toEqual({
      status: 400,
      error: 'The year parameter should not be greater than the endYear parameter; '
        + 'endYear represents the limit of the year range.'
    });
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error: Chars in year param', async () => {
    jest.spyOn(DB.prototype, 'forestryReadFiltered').mockResolvedValue({
      status: 400,
      error: 'The year param must not contain characters'
    });
    const response = await request(app).get('/forestry/year/2003abc');

    expect(response.body).toEqual({
      status: 400,
      error: 'The year param must not contain characters'
    });
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });
});

const countryNameList = ['China', 'Canada', 'Ukraine', 'France'];
const expectedDataCanada = [
  {Country: 'Canada', Year: '2014', 
    Afforestation: 0, Deforestation: 0, ForestCover: 1.5, ForestArea: 2000},
  {Country: 'Canada', Year: '2015', 
    Afforestation: 0, Deforestation: 0, ForestCover: 1.6, ForestArea: 2002},
  {Country: 'Canada', Year: '2002', 
    Afforestation: 0, Deforestation: 0, ForestCover: 1.7, ForestArea: 2004}
];
const expectedDataSameYear = [
  {Country: 'Canada', Year: '2004', 
    Afforestation: 0, Deforestation: 0, ForestCover: 2.1, ForestArea: 1001},
  {Country: 'Canada', Year: '2004', 
    Afforestation: 0, Deforestation: 0, ForestCover: 2.2, ForestArea: 1002},
  {Country: 'Canada', Year: '2004', 
    Afforestation: 0, Deforestation: 0, ForestCover: 2.3, ForestArea: 1003}
];
const expectedDataYearRange = [
  {Country: 'A', Year: '2003', 
    Afforestation: 0, Deforestation: 0, ForestCover: 0.9, ForestArea: 2030},
  {Country: 'B', Year: '2004', 
    Afforestation: 0, Deforestation: 0, ForestCover: 0.8, ForestArea: 2500},
  {Country: 'C', Year: '2006', 
    Afforestation: 0, Deforestation: 0, ForestCover: 0.7, ForestArea: 2040}
];