const request = require('supertest');
const app = require('../../api.js');
const DB = require('../../db/db.js');

jest.mock('../../db/db');

describe('GET /countries/forestry', () => {
  
  test('Test Success: Get list of Countries from the Forestry collection', 
    async () => {
      jest.spyOn(DB.prototype, 'forestryGetUniqueValues').mockResolvedValue(
        ['Canada', 'Romania', 'China', 'Laos']
      );
      const response = await request(app).get('/countries/forestry');
      expect(response.body).toEqual(
        ['Canada', 'Romania', 'China', 'Laos']
      );
      expect(response.statusCode).toBe(200);
      expect(response.type).toEqual('application/json');
    });

  test('Test Error: DB method throws an Error - Route responds with status 500', 
    async () => {
      jest.spyOn(DB.prototype, 'forestryGetUniqueValues').mockImplementation(
        () => { 
          throw new Error;
        }
      );
      const response = await request(app).get('/countries/forestry');
    
      expect(response.body).toEqual(
        { 
          status : 500, 
          error : 'Server failed to query the database'
        }
      );

      expect(response.statusCode).toBe(500);
      expect(response.type).toEqual('application/json');
    });
});

describe('GET /countries/landslide', () => {

  test('Test Success : Received list of countries in the Landslide collection', 
    async () => {
      jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
        ['Canada', 'Romania', 'China', 'Laos']
      );
      const response = await request(app).get('/countries/landslide');
      expect(response.body).toEqual(
        ['Canada', 'Romania', 'China', 'Laos']
      );
      expect(response.statusCode).toBe(200);
      expect(response.type).toEqual('application/json');
    });

  test('Test Error : DB method throws an Error - Route responds with status 500', 
    async () => {
      jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockImplementation(
        () => { 
          throw new Error;
        }
      );
      const response = await request(app).get('/countries/landslide');
    
      expect(response.body).toEqual(
        { 
          status : 500, 
          error : 'Server failed to query the database'
        }
      );

      expect(response.statusCode).toBe(500);
      expect(response.type).toEqual('application/json');
    });
});

describe('GET /countries/common', () => {

  test('Test Success: Retrieves a list of countries that intersect both collections', 
    async () => {
      jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
        ['Canada', 'Romania', 'China']
      );
      jest.spyOn(DB.prototype, 'forestryGetUniqueValues').mockResolvedValue(
        ['Canada', 'China', 'Laos']
      );
      const response = await request(app).get('/countries/common');
      expect(response.body).toEqual(
        ['Canada', 'China']
      );
      expect(response.statusCode).toBe(200);
      expect(response.type).toEqual('application/json');
    });

  test('Test Error: Landslide collection throws an error - Route responds with status 500', 
    async () => {
      jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockImplementation(
        () => { 
          throw new Error;
        }
      );
      const response = await request(app).get('/countries/common');
    
      expect(response.body).toEqual(
        { 
          status : 500, 
          error : 'Server failed to query the database'
        }
      );

      expect(response.statusCode).toBe(500);
      expect(response.type).toEqual('application/json');
    });

  test('Test Error: Forestry collection throws an error - Route responds with status 500 ', 
    async () => {
      jest.spyOn(DB.prototype, 'forestryGetUniqueValues').mockImplementation(
        () => { 
          throw new Error;
        }
      );
      const response = await request(app).get('/countries/common');
    
      expect(response.body).toEqual(
        { 
          status : 500, 
          error : 'Server failed to query the database'
        }
      );

      expect(response.statusCode).toBe(500);
      expect(response.type).toEqual('application/json');
    });

  test('Test Error: Both collections throw an error - Route responds with status 500', 
    async () => {
      jest.spyOn(DB.prototype, 'forestryGetUniqueValues').mockImplementation(
        () => { 
          throw new Error;
        }
      );
      jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockImplementation(
        () => { 
          throw new Error;
        }
      );
      const response = await request(app).get('/countries/common');
    
      expect(response.body).toEqual(
        { 
          status : 500, 
          error : 'Server failed to query the database'
        }
      );

      expect(response.statusCode).toBe(500);
      expect(response.type).toEqual('application/json');
    });
});