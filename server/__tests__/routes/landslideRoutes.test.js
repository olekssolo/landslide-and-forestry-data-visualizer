const request = require('supertest');
const app = require('../../api.js');
const DB = require('../../db/db.js');

jest.mock('../../db/db');

const sampleData = (
  {'id':621, 'country':'Canada', 'year':2012, 'category':'mudslide',
    'trigger':'unknown', 'size':'small', 'fatality':0, 'coordinates':[-121.58, 50.23]},
  {'id':513, 'country':'Canada', 'year':2015, 'category':'mudslide',
    'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
  {'id':623, 'country':'Canada', 'year':2009, 'category':'mudslide',
    'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
  {'id':625, 'country':'Canada', 'year':2008, 'category':'mudslide',
    'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
  {'id':679, 'country':'Canada', 'year':2013, 'category':'complex',
    'trigger':'rain', 'size':'medium', 'fatality':0, 'coordinates':[-123.2317, 49.5622]}
);

const countryArray = ['Canada', 'Japan', 'France', 'Mexico'];

describe('GET /landslide/country/:country', () => {
  test('Test Success : Valid country', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(sampleData);
    jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
      countryArray
    );

    const response = await request(app).get('/landslides/country/Canada');
    expect(response.body).toEqual(sampleData);
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error : Invalid country', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue([]);
    jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
      countryArray
    );

    const response = await request(app).get('/landslides/country/abc');
    expect(response.body).toEqual(
      {'status':400, 'error':'Country must be a valid country contained within the database'}
    );
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });
});

describe('GET /landslide/country/:country?year=year', () => {
  test('Test Success : Valid country and year', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(sampleData);
    jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
      countryArray
    );

    const response = await request(app).get('/landslides/country/Canada?year=2008');
    expect(response.body).toEqual(sampleData);
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Test Success : No results found', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue([]);
    jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
      countryArray
    );

    const response = await request(app).get('/landslides/country/Canada?year=2008');
    expect(response.body).toEqual(
      {'message':'0 query results', 'data':[]}
    );
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error : Valid country and invalid year', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(sampleData);
    jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
      countryArray
    );

    const response = await request(app).get('/landslides/country/Canada?year=abc');
    expect(response.body).toEqual(
      {'status':400, 'error':'Year must be a valid integer greater than 0 and less than 2017'}
    );
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error : Invalid country and valid year', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(sampleData);
    jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
      countryArray
    );

    const response = await request(app).get('/landslides/country/abc?year=2009');
    expect(response.body).toEqual(
      {'status':400, 'error':'Country must be a valid country contained within the database'}
    );
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });
});

describe('GET /landslide/year/:year', () => {
  test('Test Success : Valid year', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(
      {'id':513, 'country':'Canada', 'year':2009, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2009, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2009, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );
    jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
      countryArray
    );

    const response = await request(app).get('/landslides/year/2009');
    expect(response.body).toEqual(
      {'id':513, 'country':'Canada', 'year':2009, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2009, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2009, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error : Invalid year range', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue([]);
    jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
      countryArray
    );

    const response = await request(app).get('/landslides/year/2099');
    expect(response.body).toEqual(
      {'status':400, 'error':'Year must be a valid integer greater than 0 and less than 2017'}
    );
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error : Invalid year', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(
      {'id':513, 'country':'Canada', 'year':2009, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2009, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2009, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );
    jest.spyOn(DB.prototype, 'landslideGetUniqueValues').mockResolvedValue(
      countryArray
    );

    const response = await request(app).get('/landslides/year/abc');
    expect(response.body).toEqual(
      {'status':400, 'error':'Year must be a valid integer greater than 0 and less than 2017'}
    );
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });
});

describe('GET /landslide/year/:year?endYear=endYear', () => {
  test('Test Success : Valid start and end year', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(
      {'id':513, 'country':'Canada', 'year':2010, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2010, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2010, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );

    const response = await request(app).get('/landslides/year/2010?endYear=2010');
    expect(response.body).toEqual(
      {'id':513, 'country':'Canada', 'year':2010, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2010, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2010, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Test Success : Valid year to endYear range', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(
      {'id':513, 'country':'Canada', 'year':2010, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2011, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2012, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );

    const response = await request(app).get('/landslides/year/2010?endYear=2012');
    expect(response.body).toEqual(
      {'id':513, 'country':'Canada', 'year':2010, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2011, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2012, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error : Invalid endYear', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(
      {'id':513, 'country':'Canada', 'year':2010, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2011, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2012, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );

    const response = await request(app).get('/landslides/year/2010?endYear=2009');
    expect(response.body).toEqual(
      {'status':400, 'error':'EndYear must be a valid integer greater than or equal to 2010 and '
      + 'less than 2017'}
    );
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });

  test('Test Success : No results found', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue([]);

    const response = await request(app).get('/landslides/year/1500?endYear=1501');
    expect(response.body).toEqual(
      {'message':'0 query results', 'data':[]}
    );
    expect(response.statusCode).toBe(200);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error : Invalid year and endYear', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue([]);

    const response = await request(app).get('/landslides/year/2099?endYear=2099');
    expect(response.body).toEqual(
      {'error': 'Year must be a valid integer greater than 0 and less than 2017', 'status': 400}
    );
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error : Invalid year and valid endYear', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(
      {'id':513, 'country':'Canada', 'year':2010, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2011, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2012, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );

    const response = await request(app).get('/landslides/year/abc?endYear=2009');
    expect(response.body).toEqual(
      {'status':400, 'error':'Year must be a valid integer greater than 0 and less than 2017'}
    );
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error : Valid year and invalid endYear', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(
      {'id':513, 'country':'Canada', 'year':2010, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2011, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2012, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );

    const response = await request(app).get('/landslides/year/2004?endYear=abc');
    expect(response.body).toEqual(
      {'status':400, 'error':'EndYear must be a valid integer greater than or equal to 2004'
      + ' and less than 2017'}
    );
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });

  test('Test Error : Valid year and invalid endYear', async () => {
    // Mocking the DB class
    jest.spyOn(DB.prototype, 'landslideReadFiltered').mockResolvedValue(
      {'id':513, 'country':'Canada', 'year':2010, 'category':'mudslide',
        'trigger':'rain', 'size':'small', 'fatality':0, 'coordinates':[-117.8532, 50.3162]},
      {'id':623, 'country':'Canada', 'year':2011, 'category':'mudslide',
        'trigger':'downpour', 'size':'small', 'fatality':0, 'coordinates':[-119.22, 53.03]},
      {'id':700, 'country':'Canada', 'year':2012, 'category':'landslide',
        'trigger':'downpour', 'size':'medium', 'fatality':0, 'coordinates':[-116.4484, 51.417]}
    );

    const response = await request(app).get('/landslides/year/2004?endYear=2008abc');
    expect(response.body).toEqual(
      {'status':400, 'error':'EndYear must be a valid integer greater than or equal to 2004'
      + ' and less than 2017'}
    );
    expect(response.statusCode).toBe(400);
    expect(response.type).toEqual('application/json');
  });
});