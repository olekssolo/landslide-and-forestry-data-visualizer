const parseLandslideJson = require('../../utils/landslideDataParser.js');

describe('Landslide Data Parser',  () => {
  test('Returns array of two objects from the parsed data', 
    async () => {
      const parsedData = await parseLandslideJson(
        './__tests__/utils/landslideTestData.json');
      expect(parsedData).toEqual(expectedData);
    }
  );

  test('Return small array of parsed data that ommits an invalid row', 
    async () => {
      const parsedData = await parseLandslideJson(
        './__tests__/utils/landslideTestDataInvalid.json');
      expect(parsedData).toEqual(expectedData);
    }
  );

  test('Returns array that omits undefined rows',
    async () => {
      const parsedData = await parseLandslideJson(
        './__tests__/utils/landslideTestDataUndefined.json');
      expect(parsedData).toEqual(expectedData);
    }
  );
});

const expectedData = [{
  id: 684,
  country: 'China',
  year: 2008,
  category: 'landslide',
  trigger: 'rain',
  size: 'large',
  fatality: 11,
  coordinates: [
    107.45,
    32.5625,
  ],
}, {
  id: 956,
  country: 'United States',
  year: 2009,
  category: 'mudslide',
  trigger: 'downpour',
  size: 'small',
  fatality: 0,
  coordinates: [
    -122.663,
    45.42,
  ],
}];

