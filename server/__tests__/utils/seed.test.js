const DB = require('../../db/db');
const seedDB = require('../../utils/seed.js');

jest.mock('../../db/db');

describe('Database Seeding', () => {

  test('Seed the database with no data present', async () => {
    jest.spyOn(DB.prototype, 'connect').mockResolvedValue('Oops');
    jest.spyOn(DB.prototype, 'forestryCreateMany').mockResolvedValue(15);
    jest.spyOn(DB.prototype, 'landslideCreateMany').mockResolvedValue(15);
    jest.spyOn(DB.prototype, 'getCollectionCount').mockResolvedValue(0);
    const result = await seedDB();
    expect(result[0]).toEqual(15);
    expect(result[1]).toEqual(15);
  });

  test('Attempt to seed DB that is already seeded', async () => {
    jest.spyOn(DB.prototype, 'connect').mockResolvedValue('Oops');
    jest.spyOn(DB.prototype, 'forestryCreateMany').mockResolvedValue(15);
    jest.spyOn(DB.prototype, 'landslideCreateMany').mockResolvedValue(15);
    jest.spyOn(DB.prototype, 'getCollectionCount').mockResolvedValue(100);
    const result = await seedDB();
    expect(result[0]).toEqual(0);
    expect(result[1]).toEqual(0);
  });

});

