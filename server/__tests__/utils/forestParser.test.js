const translateForestCSVToJson = require('../../utils/forestDataParser.js');

describe('translateForestCSVToJson tests', () => {
  test('Test Success: parse data with no issues', async () => {
    const csvData = await translateForestCSVToJson('./__tests__/utils/expectedData.csv');
    const testingRow = csvData[0];

    const expectedRow = {
      Country: 'Afghanistan',
      Year: 1990,
      Afforestation: 5,
      Deforestation: 10,
      ForestCover: 1.8509940875532274,
      ForestArea: 1208.44
    };
    
    expect(testingRow).toEqual(expectedRow);
  });

  test('Test Error: parse data with wrong fields', async () => {
    const csvData = await translateForestCSVToJson('./__tests__/utils/faultyData.csv');
    // this row has strings for afforestation and deforestation
    const rowWithWrongFields = csvData[0];

    const expectedRow = {
      Country: 'Afghanistan',
      Year: 1990,
      Afforestation: 0,
      Deforestation: 0,
      ForestCover: 1.8509940875532274,
      ForestArea: 1208.44
    };

    expect(rowWithWrongFields).toEqual(expectedRow);
  });
});