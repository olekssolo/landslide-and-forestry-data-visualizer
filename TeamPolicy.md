## Team Policy

- Coordinator: Christopher Kwok
- Monitor: Oleks Sologub
- Checker: Thomas Roos

## Meeting Times

- Monday: 2:30 pm - In-person (Work)
- Tuesday: 11:30 am - In-person (Check-in/Work)
- Friday: 11:30 pm - In-person (Check-in)

## Response Delay
24 hours

## In case of Emergency

- Try making use of alternative means of communication (Discord/MiO/Outlook)
- If the teammate is unreachable, consider reworking the work-load
- Should they remain unreachable, contact the instructor

## Alternative Communication Methods

- MiO
- Outlook

## Team Expectations

- We expect teammates to show up to meetings on-time and come prepared with discussion points/concerns/progress
- We expect teammates to contribute their fair share of work to each milestone
- We expect a teammate to communicate with the rest of the group should they feel overwhelmed 
-> The group is expected to respond appropriately and accommodate within a reasonable margin
- We expect teammates to stay in communication throughout the project and keep each other updated on our progress
- We expect teammates to provide support and create an environment that focuses on positive reinforcement while recognizing the importance of constructive criticism
- We expect teammates assume everyone is acting in good faith (“most generous interpretation” of words, actions) so that disagreements don’t escalate
