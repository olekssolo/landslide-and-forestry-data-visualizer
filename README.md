# Web Project (Thomas Roos, Olek Sologub, Christopher Kwok)

> MERN (MongoDB - Express - React - Node)

**Description:** The final project for the 520 Web Development course. Focuses around developing a full stack website around 2 datasets and creating a functional/responsive app. In our specific case we created a landslide/forestry website that tells a data story between the number of landslides and the afforestation and deforestation levels of each respective country. Our website features 3 views, each containing a different representation of our data. We use a heatmap and 2 plot diagrams to demonstrate the relationship between our 2 datasets.


## Setup

To install all the dependencies and build the React app, run this from the root:

```
cd server
npm run seed
cd ..
npm run build
```

Create a `.env` file with your `ATLAS_URI`

## To run the app

### Full Stack Application

```
npm run start
```

### Proxy Client and Server

#### Setup

1. Go to the `server` folder
2. Open the `package.json` file
3. Change `line 4` to:
> const port = process.env.PORT || 3001;
4. Go to the `client` folder
5. Open the `package.json` file
6. Add the following line to the **end** of the file:
> "proxy": "http://localhost:3001"

#### Run the proxy

```
cd server 
npm run start
```
Open a new terminal
```
cd client
npm run start
```

### Run Server Tests

```
cd server
npm run test
```

### Redeployment Instructions

Your application must be in a tar.gz format. This can be downloaded from the GitLab artifacts or you may zip your own application locally.

Copy tar file into the Lightsail instance:
```
scp -r -i <pem file> <file_to_copy> bitnami@35.183.132.134:~
```

Connect to the Lightsail instance
 Either use the in browser terminal or use:
```
ssh <pem file> bitnami@35.183.132.134:~
```

Unzip your archive
```
tar -xvf <archive.tar.gz>
```

Move into your application server directory and set the NODE_ENV as follows:
```
cd server
NODE_ENV=production forever start bin/www.js
```

Restart the Apache service:
```
sudo /opt/bitnami/ctlscript.sh restart apache
```

## Structure

```
root
├── client
│   ├── public
│   │   ├── index.html
│   │   └── leaflet-heat.js
│   └── src
│       ├── assets
│       │   ├── commonCountries.json
│       │   └── forestryCountries.json
│       ├── components
│       │   ├── heatmap
│       │   │   ├── HeatLayer.js
│       │   │   └── LandslideHeatmap.js
│       │   ├── Sidebar
│       │   │   ├── Sidebar.js
│       │   │   ├── SidebarContent.js
│       │   │   └── SidebarIcons.js
│       │   ├── AppInterface.js
│       │   ├── DataAnalysisView.js
│       │   ├── ForestryGraph.js
│       │   └── LandslideView.js
│       ├── stylesheets
│       │   ├── App.css
│       │   ├── AppInterface.css
│       │   ├── DataAnalysisView.css
│       │   ├── index.css
│       │   ├── Sidebar.css
│       │   └── View.css
│       ├── App.js
│       └── index.js
├── planning
│   ├── wireframes
│   └── Proposal.md
├── server
│   ├── __tests__
│   │   ├── routes
│   │   │   ├── countryRouter.test.js
│   │   │   ├── forestRouter.test.js
│   │   │   └── landslideRouter.test.js        
│   │   └── utils
│   │       ├── forestParser.test.js
│   │       ├── landslideParser.test.js
│   │       └── seed.test.js
│   ├── bin
│   │   └── www.js
│   ├── db
│   │   └── db.js
│   ├── routes
│   │   ├── countryRouter.js
│   │   ├── forestryRouter.js
│   │   └── landslideRouter.js
│   ├── utils
│   │   ├── extractForestryCountries.js
│   │   ├── extractUniqueCountries.js
│   │   ├── fileReader.js
│   │   ├── forestDataParser.js
│   │   ├── landslideDataParser.js
│   │   ├── runSeed.js
│   │   └── seed.js
│   └── api.js
├── README.md
└── TeamPolicy.md
```
