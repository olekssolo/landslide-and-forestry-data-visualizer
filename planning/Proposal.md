# Correlation Between Landslides and Forest Cover

## Data

[Link to our Choose Data issue](https://gitlab.com/csy3dawson23-24/520/teams/TeamQ23-ThomasChrisOlek/520-project-roos-sologub-kwok/-/issues/5)

## API Endpoints

1 - Forest Cover Data by Country (Optional Param: Year Range)

2 - Forest Cover Data by Year (Optional Param: End Year)

3 - Landslide Data by Country (Optional Param: Year)

4 - Landslide Data by Year (Optional Param: End Year)

## Visusalizations

### Forest Data

- Graphical representation of the change in forest cover in a given region

### Landslide Data

- Heatmap representation of landslide data in a given region

### Data Story

The data we've chosen tells the story of the relationship between the fluctuating levels of forest cover and landslide statistics over the course of 30~40 years. A beautiful love story between trees and the earth from which they both take root from.

## Views

`Note: Add missing wireframes`

1 - Heatmap of Landslides

![Landslide Wireframe](./wireframes/Landslide_Data_Wireframe.png)

2 - Graph chart of forest cover changes

![Forestry Wireframe](./wireframes/Forestry_Data_Wireframe.png)

3 - Graphical comparison between both

![Forestry Wireframe](./wireframes/Mixed_Data_Wireframe.png)

## Functionality

- Year Range : Choose a time period to display data for (Range Slider)
- Country : Choose which country to display data for (Displays Checklist of Countries - Includes "Select All" option)

### Forest Cover

- Display Deforestation
- Display Afforestation

### Landslide View

- Landslide Size
- Intensity Selection
- Landslide Trigger
- Fatality Selector (Stretch Goal)

## Features and Priorities

- `Landslide Heatmap`
    - Year Range
    - Country
- `Forest Cover Graph`
    - Year Range
    - Country
- `Comparison Data`
    - Year Range
    - Country

## Dependencies

[Leaflet Heatmap](https://leafletjs.com/plugins.html#heatmaps)


We plan to use leaflet heatmap to represent the landslide data per country. We chose it because we have previous experience working with leaflet.


[Plotly Graphing](https://plotly.com/javascript/react/)


We plan to use plotly graphing to represent the changes in forest cover over time per country. We also plan to use it to represent the relationship between our landslide data and the changes in forest cover. We are using plotly as per our instructor's recommendation.


## Proposal Updates

### New Wireframe 
We have decided that we will have data beside the fold in a sidebar instead of presenting all the data at the same time on the page. This allows our graphs/map to take up more screen space
![Updated Generic Wireframe](./wireframes/Updated_Generic_Wireframe.png)

The sidebar will extend ontop of the presented data.
![Updated Generic Wireframe](./wireframes/Updated_Generic_Wireframe_Sidebar_Open.png)