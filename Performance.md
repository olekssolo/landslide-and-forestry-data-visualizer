# Performance of Forestry and Landslide Data Analysis

## Introduction and Methodology

<!-- Briefly state how you gathered data about app performance, and in what environment 
(which browsers, what browser versions, what kind of device, OS,
width and height of viewport as reported in the console with `window.screen) -->

In this report we will be analyzing our application and using those analytics in 
order to determine how we can improve our application's performance.
The tools that we used to analyze our application were:
  - [What Does My Site Cost](whatdoesmysitecost.com)
  - [Google Lighthouse](https://developer.chrome.com/docs/lighthouse/overview/)
  - [Google Chrome Developer Tools](https://developer.chrome.com/docs/devtools/)
  - [Firefox Developer Tools](https://firefox-source-docs.mozilla.org/devtools-user/)

The devices that we are testing on include:
  - Windows 10 Desktop PC, Google Chrome, Viewport: 1920x1080
  - Ventura 13.2.1 Macbook Air, Firefox, Viewport: 1440x800
  - Samsung Galaxy S9+, FireFox and Google Chrome, Viewport: 412x846
  - Windows 11 Laptop, Google Chrome, Viewport: 1280x760
  - Windows 11 Desktop, Firefox and Google Chrome, Viewport: 1920x1080
  

<!-- Also report overall impact on whatdoesmysitecost results before and after all your changes -->

## Baseline Performance

<!-- Summarize initial results for each tool that you used -->
### What Does My Site Cost
>![What Does My Site Cost Analysis](performance_report_assets/whatDoesMySiteCost_Before.PNG)

### Lighthouse

>![Lighthouse Performance Before](./performance_report_assets/Lighthouse_Performance_Before.PNG)
>![Lighthouse Performance Opportunities](./performance_report_assets/Lighthouse_Performace_Opportunities_Before.PNG)
>![Lighthouse Best Practices](./performance_report_assets/Lighthouse_BestPractices_Before.PNG)
>
>The Lighthouse analysis shows us the potential places where we can optimize our website. This report has shown us that we should focus on our performance instead of best practices. We determined this because we know that our best practices is due to not using HTTPS and that is not something we feel prepared to tackle for this project. We want to reduce the time for our Largest Contentful Paint, the Total Blocking Time and hopefully the speed index as well.


>![Lighthouse Treemap Before](/performance_report_assets/Lighthours_Treemap_Before.PNG)
> This image demonstrates that plotly.js  is taking up an disproportional amount of the data being transferred. In the bottom we see that 2.5mb (74%) of the plotly.js library is not being used. 

## Areas to Improve

  * Text Compression
  * Time to Largest Contentful Paint
  * Reduce the Unused Javascript

## Summary of Changes 

<!-- Briefly describe each change and the impact it had one performance (be specific). If there
was no performance improvement, explain why that might be the case -->


### Reduce Time for Largest Contentful Paint

**Lead**: Thomas Roos

>My goal was to reduce the `largest contentful paint` from its original lacking 3.1s, analyzed by the `lighthouse` report. Because our app focuses on the heatmap as its first viewpoint for the user when visiting our website, we felt it was necessary to decrease this time, with the heatmap as a focus for pre-loading. I ran into several issues with this optimization, however, initially I intended to use a large screenshot of our map but the quality was appalingly bad. Not to mention there were several implications to take into account, as I had to ensure that the image was large enough to cover the entire viewport, even on different devices with larger or smaller screens. While the smaller screens weren't of much concern, the larger screens were. I was going to use a absolute positioning on the image to center it, however if the user had a larger viewport, this would look quite ugly if it was too small, plus a white flicker would jump in and out as React renders in our components. As discussed, my implementation of `leaflet's streetview API` was rather experimental, and although bad implementation, allowed me to further investigate and learn about React rendering, image processing, pre-loading, and leaflet tile fetching. Using a list of tiles typically used as the start of the heatmap's initial render, I was able to emulate the heatmap in a pre-load style. Because the library splits the viewport map into several chunks, I had to make convenient usage of the heatmap library that arranges/lines up the images in the correct order based on attributes. The image pre-loading allows the heatmap's initial set of tiles have their external reference be cached, and as a result, the loading time for the "secondary" `(react)` load is significantly faster. This allowed us to reduce our `LCP` by a significant margin and vastly increase our lighthouse speed index, as well as overall score. A clever trick unrelated to performance that Chris came up with was to change the background color to the same shade of blue as our map, therefore eliminating the white flashing that appeared when React rendered in and removed the pre-loaded map.
> ![Lighthouse Performance After Pre-Loading](./performance_report_assets/LCP.png)

### Enable Text Compression

**Lead**: Oleksandr Sologub

> My goal was to enable text compression in our application to reduce loading time. This change is very simple, but helps with loading times by compressing responses sent by Express. The browser then automatically decompresses the responses. It reduced total loading time by a substantial margin; it took up around 1.23 seconds initially.
  > Additionally, I took a look at the other optimization suggestions Lighthouse offered, and tried implementing cache control headers. However, the error seemed to source from something else, and the cache control did not improve nor worsen performance.

### Reduce Unused Javascript

**Lead**: Christopher Kwok

>My main target was reducing the unused Javascript so I did this by using a [smaller distribution of plotly.js](https://www.npmjs.com/package/plotly.js-basic-dist-min). This lead to a reduction in Unused Javascript which I intended but also a massive reduction in the `Total Blocking Time` which I had not considered. 
>The reduction in `Unused Javascript` comes from the version of Plotly I now use being  .975MB instead of the 3.4MB from the full version of Plotly. That being said, there is still only 26% of the library being used but I was unable to find a smaller version of Plotly at this time.
>The `Total Blocking Time` being reduced so much I am less certain of but I believe it is because there is much less in the Plotly library that needs to be parsed now so much less time is wasted on it.
> ![Lighthouse Performance After Changing Plotly Libraries](./performance_report_assets/Lighthouse_Performance_After_Plotly_Minified.PNG)
> ![Lighthouse Treemap After Changing Plotly Libraries](./performance_report_assets/Lighthouse_Treemap_After_Min_Plotly.PNG)


### Cache Fetched Data

**Contributors**: Oleksandr, Thomas

>### Thomas
>
> I cached the `Landslide API` fetching. I changed the useEffect to be dependent on the input being changed. The function filters through the range of years to fetch in segments of 3 years. This is to optimize our `scalability`, by reducing the number of fetches, but also ensuring that not too much data is grabbed in a single fetch. This was done to prevent a larger scale project with millions of rows instead of thousands from being too slow. This optimization greatly decreased the loading time for the initial render, and also decreased the time needed when switching between views. The data is cached in the `session storage`, and is cleared once the session has ended to prevent stale data. This change both affected navigation between views, but also optimized the initial render, thus improving our lighthouse score and other evaluations of website speed index.
> 
>### Oleksandr
>
> I cached the `Forestry API` and both the `Landslide and Forestry APIs` in the forestry view and data analysis view, respectively. Since the two views would use caching in a similar manner, I implemented caching in both of them. First, the IIFE checks if there is an entry in the session storage that matches the pattern "forestry_{country}" or "landslides_{country}" (where country is the current country state variable), then it fetches from the database if necessary, or uses the entry in session storage. I also cached the list of countries from the `Countries API` in a similar fashion. As with Thomas's caching, this improves speed of navigation between views, as well as the initial render. Additionally, since the data analysis view uses the same data as the forestry view, it helps with the speed of loading of the data analysis view as well.

## Final Performance

![Lighthouse Overall Release](./performance_report_assets/Lighthouse_Overall_Release.PNG)
![Lighthouse Performance Release](./performance_report_assets/Lighthouse_Performance_Release.PNG)
![Lighthouse Performance Opportunities Release](./performance_report_assets/Lighthouse_Performace_Opportunities_Release.PNG)
![Lighthouse Best Practices Release](./performance_report_assets/Lighthouse_BestPractices_Release.PNG)
![Lighthouse Treemap Release](./performance_report_assets/Lighthouse_Treemap_Release.PNG)
![What Does My Site Cost Release](./performance_report_assets/whatDoesMySiteCost_After.PNG)

## Conclusion

<!-- Summarize which changes had the greatest impact, note any surprising results and list 2-3 main 
things you learned from this experience. -->

The two changes that we noticed the greatest impacts from was reducing the unused Javascript and preloading the tile map for our heatmap. We were surprised how much we were able to reduce the total blocking time as our primary focuses were to reduce the weight of the application and the largest contentful pain. One of the things that we have learned are that small changes can be very impactful. We learned about image preloading and the effects it has on image caches and how that will change the time for the first contentful paint and the largest contentful paint. This also forced us to learn about the way that React orders it's rendering and how it affects vanilla HTML in an application. We also learned to look for smaller distributions of the packages that we want to use, as using a smaller distribution of Plotly.js had a huge impact on our performance.